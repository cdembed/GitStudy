# Git安装

用户使用Git工具在本地对代码进行管理、提交、更新等操作。

### Windows 7及以上
[Git Win7及以上](https://git-scm.com/download/win)

### Windows XP
点击[Git-2.10.0-32-bit.exe](https://github.com/git-for-windows/git/releases/download/v2.10.0.windows.1/Git-2.10.0-32-bit.exe)下载


### Ubuntu

        $ sudo apt-get install git