# Git更新远程仓库代码到本地

### 查看远程分支
        $ git remote -v
        
### 从远程获取最新版本到本地
这里需要新建一个本地分支 **temp**

        $ git fetch origin master:temp
        
### 比较本地仓库当前分支与下载的temp分支的差异
使用如下命令来比较本地代码与刚刚从远程下载下来的代码的区别：
        
        $ git diff temp
        
### 合并temp分支到本地当前的分支
对比区别之后，如果觉得没有问题，可以使用如下命令进行代码合并：

        $ git merge temp
        
### 删除temp分支
如果 **temp** 分支不想要保留，可以使用如下命令删除该分支：

        $ git branch -d temp
