# Git简明教程

本教程是一个简要的命令记录，方便随时查看一些常用的命令。需要深入了解的请查阅《Github入门与实践》第四章节。

### 初始化一个新的代码仓库
        $ git init
        
### 查看当前仓库的状态信息
这是一个十分有用的命令，工作树和仓库在被操作的过程中，状态会不断变化。在Git操作过程中常用此命令来查看当前状态。

        $ git status
        
### 向暂存区中添加文件
暂存区是提交前的一个临时区域。

        $ git add
例：  

        $ git add .     // 添加当前目录下所有文件
        
        $ git add README.md     // 添加单个文件
   
   
### 提交修改
将暂存区中的文件变动信息提交至代码仓库的历史记录中。

        $ git commit 
例：

        $ git commit -m "This is a modify message."
        
### 查看提交记录
查看完整历史提交记录

        $ git log

查看历史提交记录的第一行

        $ git log pretty=short
        
只显示指定目录、文件的提交记录

        $ git log 目录名或文件名
        
显示文件提交前后的差异

        $ git log -p 文件名
        
### 分支操作
显示分支，带 * 号的为当前分支

        $ git branch
        
创建新分支并切换至新分支
        
        $ git branch -b 新分支名称
        
切换当前所在分支

        $ git checkout 分支名称
        
切换回上一分支

        $ git checkout -
        
合并分支

        $ git merge
        
### 回滚与前进
回滚到之前的版本

        $ git reset --hard 节点哈希值（通过git log 查看）
        
回滚后要前进到之后的版本

        $ git reflog        // 查看操作日志，获得之后版本的哈希值
        $ git reset --hard 节点哈希值